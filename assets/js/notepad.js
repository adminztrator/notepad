$('.msgBody').jqte();
$('#editMsgBody').jqte();

var notes = [];
				
$(document).ready(function(){
	$.ajax({
	  url: "note",
	  success: function(data)
	  {
		console.log(data);
		notes = data;
		loadNotes(data);
		$('body').fadeIn(1000);
	  }
	});
});
	
function createNote()
{
	//alert("create");
	$("#notes").fadeOut(500);
	$("#note").fadeIn(500);
}
				
function previewNote()
{
	//$("#preview").fadeOut(500);
	var title = "<div align='center'>";
	title += "<h3>" + $("#noteTitle").val() + " (Preview)</h3>";
	title += "<p>" + $("#noteSubtitle").val() + "</p>";
	title += "</div>";
	$("#preview").html("<hr>" + title + $(".msgBody").html() + "<hr>");
	$("#preview").fadeIn(1000);
}
				
function cancelNote()
{
	$("#preview").fadeOut(250);
	$('#note').fadeOut(250);
	$('#notes').fadeIn(500);
	$("#preview").html("");
	$(".jqte_editor").html("");
	//$("#preview").css("display", "none");
}
				
function submitNote()
{
	$.ajax({
	  type: "POST",
	  url: "note/add",
	  data: {
		title: $("#noteTitle").val(),
		subtitle: $("#noteSubtitle").val(), 
		msgBody: $('.msgBody').html()
	  },
	  success: function(data)
	  {
		if (data == "Error")
		{
			alert("Couldn't add note to database!");
		}
		else if (data == "Success")
		{
			$.ajax({
			  url: "note",
			  success: function(data)
			  {
				console.log(data);
				notes = data;
				loadNotes(data);
				cancelNote();
			  }
			});
		}
	  }
	});
}
				
function loadNotes(data)
{
	var template = '<article class="post"><header><div class="title"><h2><a href="#">{{title}}</a></h2><p>{{subtitle}}</p>';
	template += '</div><div class="meta"><time class="published" datetime="{{created}}"></time></div></header>';
	template += '<p>{{messagebody}}</p><footer>';
	template += '<ul class="actions" align="right">';
	template += '<li><a href="#" class="button big {{id}}" onclick="downloadPdf(this.className);">Download PDF</a></li>';
	template += '<li><a href="#" class="button big {{id}}" onclick="editNote(this.className);">Edit</a></li>'
	template += '<li><a href="#" class="button big {{id}}" onclick="deleteNote(this.className);">Delete</a></li>'
	template += '</ul></footer></article>';
	var html = "";
	data.lower = function () {
       return function formatDate(date, render)
	   {
			return render(new Date(date).toDateString());
	   }
    };
	for (var i = 0; i < data.length; i++)
	{
		html += Mustache.to_html(template, data[i]);
	}
	$('#notes').html(html);
	$( "time" ).each(function( time ) {
	  $( this ).html(formatDate($( this ).attr('datetime')));
	});
}

function deleteNote(classStr)
{
	var id = getNoteId(classStr);
	console.log("note/" + (id).toString());
	$.ajax({
	  type: "DELETE",
	  url: ("note/" + id),
	  data: {},
	  success: function(data)
	  {
		if (data == "Error")
		{
			alert("Couldn't delete note!");
		}
		else if (data == "Success")
		{
			location.reload();
		}
	  }
	});
}

function editNote(classStr)
{
	var id = getNoteId(classStr);
	for (var i = 0; i < notes.length; i++)
	{
		if (parseInt(notes[i].id) == id)
		{
			$("#editNoteTitle").val(notes[i].title);
			$("#editNoteSubtitle").val(notes[i].subtitle);
			$("#editMsgBody").val(notes[i].messageBody);
			$("#submitEdit").addClass(notes[i].id);
			$("#notes").fadeOut(500);
			$("#edit").fadeIn(500);
		}
	}
}

function downloadPdf(classStr)
{
	var id = getNoteId(classStr);
	for (var i = 0; i < notes.length; i++)
	{
		if (parseInt(notes[i].id) == id)
		{
			var doc = new jsPDF()

			doc.setFontSize(28);
			doc.text(notes[i].title, 10, 10);
			doc.setFontSize(14);
			doc.text(notes[i].subtitle + "\nPublished: " + formatDate(notes[i].created), 10, 20);
			doc.setFontSize(12);
			var messageBody = notes[i].messagebody;
			messageBody.replace("/<(?:.|\n)*?>/gm", '');
			doc.text(messageBody, 40, 40);
			doc.save(notes[i].title + '.pdf');
		}
	}
}

function getNoteId(classStr)
{
	var tokens = classStr.split(" ");
	for (var i = 0; i < tokens.length; i++)
	{
		if (parseInt(tokens[i]) >= 0)
		{
			console.log(tokens[i]);
			return parseInt(tokens[i]);
		}
	}
}

function formatDate(date)
{
	return (new Date(date).toDateString());
}

function submitEdits(classStr)
{
	alert("submitted edit");
	var id = getNoteId(classStr);
	$.ajax({
	  type: "PUT",
	  url: ("note/" + (id).toString()),
	  data: {
		title: $("#editNoteTitle").val(),
		subtitle: $("#editNoteSubtitle").val(), 
		msgBody: $("#editMsgBody").html()
	  },
	  success: function(data)
	  {
		if (data == "Error")
		{
			alert("Couldn't add note to database!");
		}
		else if (data == "Success")
		{
			location.reload();
		}
	  }
	});
	
}

