import React from 'react';

class Posts extends React.Component {
   constructor(props) {
      super(props);
   }
   
   render() {
	   var posts = []
	   for (var i = 0; i < this.props.data.length; i++){
		   var currentPost = this.props.data[i];
		   posts.push(<Post />);
	   }
	   return {posts};
   }
}

class Post extends React.Component {
   constructor(props) {
      super(props);
   }
   
   render() {
      return (
		<article class="post">
			<header>
				<div class="title">
				<h2><a>{this.props.title}</a></h2>
				<p>{this.props.subtitle}</p>
				</div>
				<div class="meta">
				<time class="published" datetime="2015-10-25">{this.props.created}</time>
				</div>
			</header>
			<p>{this.props.messagebody}</p>
			<footer>
				<ul class="actions" align="right">
					<li><a class="button big 1" onclick="alert(this.className);">Download PDF</a></li>
					<li><a class="button big 1" onclick="alert(this.className);">Edit</a></li>
					<li><a class="button big 1" onclick="alert(this.className);">Delete</a></li>
				</ul>
			</footer>
		</article>
      );
   }
}

export default Posts;