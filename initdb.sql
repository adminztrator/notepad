CREATE TABLE notes(
	id 			SERIAL,
	title		varchar,
	subtitle 	varchar,
	messagebody varchar,
	created 	date default current_date
);