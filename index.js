var express = require('express')
var app = express()
var fs = require('fs');
var pg = require('pg');
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/assets', express.static('assets'))

app.get('/', function (req, res) {
	fs.readFile('index.html', 'utf8', function (err, data) {
	  if (err) {
		return console.log(err);
	  }
	  res.send(data);
	});
})

app.post('/note/add', function (req, res) {
	var title = req.body.title,
        subtitle = req.body.subtitle,
		body = req.body.msgBody;
	
	var query = "INSERT INTO notes(title, subtitle, messagebody) ";
	query += "values('"+title+"', '"+subtitle+"', '"+body+"');";
	pg.connect(process.env.DATABASE_URL, function(err, client, done) {
		client.query(query, function(err, result) {
		  done();
		  if (err)
		  { 
			res.send("Error"); 
		  }
		  else
		  { 
			res.send("Success");
		  }
		});
	});
})

app.get('/note/', function (req, res) {
	pg.connect(process.env.DATABASE_URL, function(err, client, done) {
		client.query('SELECT * FROM notes order by created DESC;', function(err, result) {
		  done();
		  if (err)
		  { 
			res.send("Error"); 
		  }
		  else
		  { 
			res.send(result.rows);
		  }
		});
	});
})

app.get('/note/:id', function (req, res) {
	pg.connect(process.env.DATABASE_URL, function(err, client, done) {
		client.query('SELECT * FROM notes WHERE id='+req.params.id + ';', function(err, result) {
		  done();
		  if (err)
		  { 
			res.send("Error"); 
		  }
		  else
		  { 
			res.send(result.rows);
		  }
		});
	});
})

app.put('/note/:id', function (req, res) {
	var title = req.body.title,
        subtitle = req.body.subtitle,
		body = req.body.msgBody;
	
	var query = "UPDATE notes SET title = '" + title + "', subtitle = '" + subtitle;
	query += "', messagebody = '" + body + "' WHERE id=" + req.params.id + ";";
	console.log(query);
	pg.connect(process.env.DATABASE_URL, function(err, client, done) {
		client.query(query, function(err, result) {
		  done();
		  if (err)
		  { 
			res.send("Error"); 
		  }
		  else
		  { 
			res.send("Success");
		  }
		});
	});
})

app.delete('/note/:id', function (req, res) {
	var query = "DELETE * FROM notes WHERE id=" + req.params.id + ";";
	console.log(query);
	pg.connect(process.env.DATABASE_URL, function(err, client, done) {
		client.query(query, function(err, result) {
		  done();
		  if (err)
		  { 
			res.send("Error"); 
		  }
		  else
		  { 
			res.send("Success");
		  }
		});
	});
})
app.listen(process.env.PORT || 3000, function () {
  console.log('Example app listening on port ' + process.env.PORT || 3000)
})